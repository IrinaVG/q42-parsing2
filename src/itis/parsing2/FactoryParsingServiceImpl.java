package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class FactoryParsingServiceImpl implements FactoryParsingService {
    public String str;



    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {
        Factory factory = new Factory();
        Field[] fields = factory.getClass().getDeclaredFields();
        String title = null;
        String organizationChiefFullName = null;
        String description = null;
        Long amountOfWorkers;
        ArrayList<String> departments = new ArrayList<>();
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(factoryDataDirectoryPath + "/factory-data-1-1");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        try {
            str = bufferedReader.readLine();
            str = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (str != null) {
            str = str.replaceAll("\"", "");
            String[] name = str.split(":");

            if (name[0].equals("title")) {
                title = name[1];
                try {
                    fields[0].setAccessible(true);
                    fields[0].set(factory, title);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            if (name[0].equals("organizationChiefFullName")) {
                organizationChiefFullName = name[1];
                try {
                    fields[1].setAccessible(true);
                    fields[1].set(factory, organizationChiefFullName);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            if (name[0].equals("description")) {
                description = name[1];
                try {
                    fields[2].setAccessible(true);
                    fields[2].set(factory, description);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            if (name[0].equals("32211")) {
                String n;
                n = name[1];
                n = n.replaceAll(" ", "");
                amountOfWorkers = Long.parseLong(n);
                try {
                    fields[3].setAccessible(true);
                    fields[3].set(factory, amountOfWorkers);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            if (name[0].equals("departments")) {
                name[1] = name[1].replace("[", "");
                name[1] = name[1].replace("]", "");
                name[1] = name[1].replaceAll(" ", "");
                String[] n = name[1].split(",");
                departments.add(n[0]);
                departments.add(n[1]);
                try {
                    fields[4].setAccessible(true);
                    fields[4].set(factory, departments);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

            try {
                str = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return factory;
    }

}
